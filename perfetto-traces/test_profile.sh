#!/bin/bash
#set -e

USER='igor'
BASE_ADDR='/home/igor'
PROJECTS_ADDR="$BASE_ADDR/projects"
DEQP_VK_PATH="$PROJECTS_ADDR/VK-GL-CTS/build/external/vulkancts/modules/vulkan/deqp-vk"
VIRGL_PATH="$PROJECTS_ADDR/virglrenderer/build/vtest/virgl_test_server"
PERFETTO_TRACES_PATH="$PROJECTS_ADDR/perfetto-traces"
TRACE_OUT="$BASE_ADDR/clipboard/trace"
ICD_BASE_ADDR="$BASE_ADDR/.local/share/vulkan/icd.d"
GUEST_DRIVER_ICD="$ICD_BASE_ADDR/virtio_icd.x86_64.json"
HOST_DRIVER_ICD="$ICD_BASE_ADDR/intel_icd.x86_64.json"
VIRGLREDERER_FLAGS='--venus --use-egl-surfaceless --no-fork'
OPTS='--timestamp sec --timeout 60'

PERFETTO_BIN_FOLDER="$PROJECTS_ADDR/perfetto/build"
QUIET_1=''
QUIET_2=''
QUIET_3=''
CASE=''

while [[ "$#" -gt 0 ]]; do
	case $1 in
		-c|--case) CASE="$2"; shift ;;
		-p|--perfetto-folder) PERFETTO_BIN_FOLDER="$2"; shift ;;
		-q|--quiet)
			case $2 in
				""|"1") QUIET_1='&> /dev/null';;
				"2") QUIET_1='&> /dev/null'; QUIET_2='1> /dev/null';;
				"3") QUIET_1='&> /dev/null'; QUIET_2='&> /dev/null';;
				"4") QUIET_1='&> /dev/null'; QUIET_2='&> /dev/null'; QUIET_3='1> /dev/null';;
				"5") QUIET_1='&> /dev/null'; QUIET_2='&> /dev/null'; QUIET_3='&> /dev/null';;
				*) echo "Unknown parameter passed to -q: $2"; exit 1 ;;
			esac
			shift ;;
		*) echo "Unknown parameter passed: $1"; exit 1 ;;
	esac
	shift
done

if [ "$CASE" == '' ]; then
	echo "Error: You need pass -c/--case <case_name>"
	exit -10
fi

if [ "$EUID" -ne 0 ]; then
  echo "Please run as root"
  exit
fi

su -c "$PERFETTO_BIN_FOLDER/traced $QUIET_1" - $USER & TRACED_PID=$!
sleep .5
su -c "$PERFETTO_BIN_FOLDER/traced_probes --reset-ftrace $QUIET_1" - root & PROBES_PID=$!
sleep .8
su -c "$PERFETTO_BIN_FOLDER/perfetto --txt -c $PERFETTO_TRACES_PATH/cfg -o $TRACE_OUT $QUIET_1" - $USER & PERFETTO_PID=$!

printf "VK_ICD_FILENAMES=$HOST_DRIVER_ICD $VIRGL_PATH $VIRGLREDERER_FLAGS $QUIET_2\n\n"
su -c "VK_ICD_FILENAMES=$HOST_DRIVER_ICD $VIRGL_PATH $VIRGLREDERER_FLAGS $QUIET_2" - $USER & REDERER_PID=$!

#echo "./record_cros_trace --gpu -b 50000 -p "$PERFETTO_BIN_FOLDER/perfetto" -o $TRACE_OUT"
#./record_cros_trace --gpu -b 50000 -p "$PERFETTO_BIN_FOLDER/perfetto" -o $TRACE_OUT -t 800 & RECORD_SCRIPT_PID=$!

sleep .5

printf "VK_ICD_FILENAMES=$GUEST_DRIVER_ICD VN_DEBUG=vtest MESA_GLSL_CACHE_DISABLE=1 $DEQP_VK_PATH --deqp-case=$CASE\n\n"
su -c "VK_ICD_FILENAMES=$GUEST_DRIVER_ICD VN_DEBUG=vtest MESA_GLSL_CACHE_DISABLE=1 $DEQP_VK_PATH --deqp-case=$CASE $QUIET_3" - $USER

wait $PERFETTO_PID

printf "\nStoping PIDs $REDERER_PID(Virglrenderer) $PROBES_PID(traced_probes) $TRACED_PID(traced)\n"
su -c "killall -15 traced_probes $QUIET_2" - root
su -c "kill -INT $REDERER_PID $TRACED_PID $QUIET_2" - root

exit 0
