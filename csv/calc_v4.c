#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <getopt.h>
#include <errno.h>
#include <string.h>
#include <math.h>

#define HELP_MESSAGE													\
    "Usage:\tcalc.exe -g 5 <csv_file> -n radv:venus\n"					\
	"About: This program servers a specific purpose. Get a csv with "	\
	"dEQP resuts with the times and caculate the average and ratio.\n"	\
    "Options:\n"														\
    "\t-g,\t--group-size\t Results groups size.\n"						\
    "\t-n,\t--group-name\t Generate header with groups names.\n"		\
    "\t-h,\t--help\t\t Show This Message.\n\n"

// Inicialization of long options of opt
#define LONG_OPTIONS													\
	{																	\
		{"group-size", required_argument, NULL, 'g'},					\
		{"groups-name", optional_argument, NULL, 'n'},					\
		{"help", no_argument, NULL, 'h'},								\
		{0, 0, 0, 0}													\
	}

#define INPUT_BUFFER_SIZE 1 << 22 // 4 MB
#define OUTPUT_BUFFER_SIZE 1 << 24 // 16 MB

#define MIN(Y, X) ((Y) < (X) ? (Y) : (X))

#define ERROR_HANDLER(ret, label)					\
	if (errno) {									\
		perror("The following error occurred: ");	\
		ret = -errno;								\
		goto label;									\
	}

#define write_line(output_buffer, len, cur_line, average, std_dev, ratio)						\
	snprintf(&output_buffer[len], (OUTPUT_BUFFER_SIZE) - (len),									\
			"%s,%lf,%lf,%lf,%lf,%lf,%lf,%lf\n", cur_line, average[0], std_dev[0],				\
			std_dev[0] / average[0], average[1], std_dev[1], std_dev[1] / average[1], ratio);

void
read_elapsed_times(double elapsed_times[], size_t group_size, char **field)
{
	char *p_end;

	for (int i = 0; i < group_size; i++) {
		elapsed_times[i] = strtod(*field, &p_end);
		if (errno)
			break;
		// set field to the beginning of next field ignoring ','
		*field = p_end + 1;
	}
}

double
calculate_average(double elapsed_times[], size_t group_size, char **field)
{
	double average = 0;

	for (int i = 0; i < group_size; i++)
		average += elapsed_times[i] / group_size;

	return average;
}

double
calcilate_std_dev(double elapsed_times[], size_t group_size, double mean)
{
	double tmp = 0;

	for (int i = 0; i < group_size; i++)
		tmp += (elapsed_times[i] - mean) * (elapsed_times[i] - mean);

	return sqrt(tmp / (group_size - 1));

}

int
main(int argc, char *argv[])
{
	size_t cur_file_pos = 0, bytes_read, group_size = 0, out_len = 0;
	char *input_buffer, *output_buffer, *group_name[2] = { NULL, NULL };
	struct option longOptions[] = LONG_OPTIONS;
	int option = 0, ret = 0;
	double *elapsed_times;
	FILE *csv = NULL;

	while (option != -1) {
		option = getopt_long(argc, argv, "n:g:h", longOptions, NULL);
		switch (option){
		case 'g':
			group_size = atoi(optarg);
			break;
		case 'n':
			group_name[1] = strpbrk(optarg, ":");
			if (group_name[1])
				group_name[1] += 1;
			group_name[0] = strtok(optarg, ":");
			break;
		case 'h':
			fprintf(stderr, HELP_MESSAGE);
			exit(EXIT_SUCCESS);
		case '?':
			// flush opt message
			fflush(stderr);
			fprintf(stderr, "Try --help\n");
			exit(EXIT_FAILURE);
		}
	}

	if (!group_size) {
		fprintf(stderr, "Invalid group size '%d'\n", group_size);
		exit(EXIT_SUCCESS);
	}

	if (!group_name[0] != !group_name[1])  {
		fprintf(stderr, "Invalid group name format '%s'\n", group_name[0]);
		exit(EXIT_SUCCESS);
	}

	if (argc - optind == 0) {
			fprintf(stderr, "No input provided\n");
			exit(EXIT_FAILURE);
	}

	csv = fopen(argv[optind], "r");
	if (!csv) {
		fprintf(stderr, "Could not open '%s'\n", argv[optind]);
		ret = -10;
		goto exit_program;
	}

	output_buffer = malloc(OUTPUT_BUFFER_SIZE);
	if (!output_buffer) {
		fprintf(stderr, "Could not allocate output buffer memory, buy more RAM please\n");
		ret = -ENOMEM;
		goto close_csv;
	}

	input_buffer = malloc(INPUT_BUFFER_SIZE);
	if (!input_buffer) {
		fprintf(stderr, "Could not allocate input buffer memory, buy more RAM please\n");
		ret = -ENOMEM;
		goto free_output_buffer;
	}

	elapsed_times = malloc(2 * group_size * sizeof(double));
	if (!elapsed_times) {
		fprintf(stderr, "Could not allocate a internal buffer, buy more RAM please\n");
		ret = -ENOMEM;
		goto free_input_buffer;
	}

	if (group_name[0]) {
		printf("dEQP test name");
		for (int i = 0; i < 2; i++)
			for (int j = 0; j < group_size; j++)
				printf(",%s result round %d", group_name[i], j);
		for (int i = 0; i < 2; i++) {
			printf(",%s average", group_name[i]);
			printf(",%s standard deviation", group_name[i]);
			printf(",%s standard deviation divided by average", group_name[i]);
		}
		printf(",%s/%s performance ratio\n", group_name[0], group_name[1]);
	}

	// Set the last byte as a guard
	input_buffer[INPUT_BUFFER_SIZE - 1] = '\0';

	// Read the buffer size -1, therefore preserving the guard
	bytes_read = fread(input_buffer, sizeof(char), INPUT_BUFFER_SIZE - 1, csv);
	while (bytes_read != 0) {
		char *cur_line;
		int i;

		// Find the position after the last new line and clears the content after it
		// (I don't want deal with a incomplete line)
		for (i = MIN(bytes_read - 1 , INPUT_BUFFER_SIZE - 2); i > 0 && input_buffer[i] != '\n'; i--)
			input_buffer[i] = '\0';
		// clean the last '\n'
		input_buffer[i] = '\0';
		// Update the current file position
		cur_file_pos += i + 1;
		// fseek to the position after the last new line
		fseek(csv, cur_file_pos, SEEK_SET);

		cur_line = strtok(input_buffer, "\n");
		while (cur_line != NULL) {
			double average[2], std_dev[2], ratio;
			size_t bytes_wrote;
			char *field;

			// Ignore the test name and the comma
			field = strchr(cur_line, ',') + 1;
			// Calculate average from the first group
			read_elapsed_times(elapsed_times, group_size * 2, &field);
			ERROR_HANDLER(ret, free_elapsed_times);
			// Calculate average from the seccond group
			average[0] = calculate_average(elapsed_times, group_size, &field);
			ERROR_HANDLER(ret, free_elapsed_times);
			average[1] = calculate_average(&elapsed_times[group_size], group_size, &field);
			ERROR_HANDLER(ret, free_elapsed_times);
			std_dev[0] = calcilate_std_dev(elapsed_times, group_size, average[0]);
			ERROR_HANDLER(ret, free_elapsed_times);
			std_dev[1] = calcilate_std_dev(&elapsed_times[group_size], group_size, average[1]);
			ERROR_HANDLER(ret, free_elapsed_times);
			ratio = average[1] / average[0];

			bytes_wrote = write_line(output_buffer, out_len, cur_line, average, std_dev, ratio);
			if (out_len + bytes_wrote >= OUTPUT_BUFFER_SIZE) {
				write(STDOUT_FILENO, output_buffer, out_len);
				out_len = write_line(output_buffer, 0, cur_line, average, std_dev, ratio);
			} else {
				out_len += bytes_wrote;
			}

			cur_line = strtok(NULL, "\n");
		}

		bytes_read = fread(input_buffer, sizeof(char), INPUT_BUFFER_SIZE - 1, csv);
	}

	write(STDOUT_FILENO, output_buffer, out_len);

	// check for any errors
	if (ferror(csv)) {
		fprintf(stderr, "Error while performing the '%s' file read\n", argv[optind]);
		ret = -EIO;
	}

free_elapsed_times:
	free(elapsed_times);
free_input_buffer:
	free(input_buffer);
free_output_buffer:
	free(output_buffer);
close_csv:
	fclose(csv);
exit_program:
	return ret;
}
