#!/bin/bash

set -e

INTERFACE=$(ip link | cut -d ' ' -f 2 | tail -n 2 | head -n 1 | sed 's/\([a-z0-9]*\):/\1/')
echo "############### Interface = $INTERFACE ###############"
sed -i "s/INTERFACE/$INTERFACE/g" /etc/network/interfaces
mv /root/resolv.conf /etc/
ifdown $INTERFACE && ifup $INTERFACE

passwd -d root
