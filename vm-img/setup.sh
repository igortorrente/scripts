#!/bin/bash

set -e

USER=igor

groupadd $USER
useradd -m -g $USER -G sudo,render,video -p '' $USER
chage -M -1 $USER
passwd -d $USER

mkdir /home/$USER/.var/vim-undo-dir -p
mkdir /root/.var/vim-undo-dir -p

rm /var/lib/dbus/machine-id
dbus-uuidgen --ensure=/var/lib/dbus/machine-id

tar -xf /root/basic.tar.xz -C /root
cp -r /root/.z* /root/.vim /root/.p10k.zsh /home/$USER
mv /root/sources.list /etc/apt/
mv /root/interfaces /etc/network/
rm /root/basic.tar.xz
chown root:root --recursive /root
chown $USER:sudo --recursive /home/$USER

apt update

apt upgrade -y

apt install sudo vim zsh openssh-server meson gcc g++ python3 git cmake curl python3-setuptools python3-mako \
pkg-config llvm-dev libelf-dev libbison-dev flex libwayland-dev wayland-protocols libxdamage-dev libxcb-glx0-dev \
libxcb-shm0-dev libx11-xcb-dev libxcb-dri2-0-dev libxcb-dri3-dev libxcb-present-dev libxshmfence-dev socat \
libxxf86vm-dev libxrandr-dev libdrm-dev libexpat1-dev libprotoc-dev protobuf-compiler libepoxy-dev libvulkan-dev \
libwayland-egl-backend-dev libxcb-render-util0-dev libxcb-render0-dev libgbm-dev libxcomposite-dev vulkan-tools -y

apt install pahole libncurses5-dev make bc libssl-dev liblz4-tool -y

apt install libpixman-1-dev libxcb-composite0-dev libxkbcommon-dev mold -y

sed -i 's/#PermitRootLogin.*$/PermitRootLogin yes/g' /etc/ssh/sshd_config
sed -i 's/#PermitEmptyPasswords.*$/PermitEmptyPasswords yes/g' /etc/ssh/sshd_config

usermod --shell /usr/bin/zsh $USER
usermod --shell /usr/bin/zsh root
