# Crosvm image scripts

These files are here so we can create a vm image for crosvm more easily.

####  Image creation

Update the `setup.sh` with your username.

``` console
$ virt-builder debian-11 --copy-in $PWD/basic.tar.xz:/root --run setup.sh --hostname crosvm-test --format qcow2 --firstboot first_boot.sh --size 40G -o ./rootfs.qcow2
```

#### Kernel extraction

``` console
$ virt-builder --get-kernel ./rootfs.qcow2 -o .
```

#### Crosvm build

``` console
$ PKG_CONFIG_PATH=/path/to/virglrenderer.pc/pkgconfig/ cargo build --release --features 'gpu x wl-dmabuf virgl_renderer virgl_renderer_next'
```

#### Crosvm run

Setup the network using the script inside the crosvm folder.

``` console
$ ./tools/examples/setup_network
```

``` console
$ VK_ICD_FILENAMES=/path/to/<vendor>_icd.x86_64.json DISPLAY=:0 LD_PRELOAD=$LD_PRELOAD:/path/to/libvirglrenderer.so.1 crosvm run --mem 4096 --cpus 4 --disable-sandbox --no-legacy --tap-name crosvm_tap --gpu vulkan=true,gles=false,backend=virglrenderer,egl=true,surfaceless=true --wayland-sock $XDG_RUNTIME_DIR/wayland-0 -s $XDG_RUNTIME_DIR/crosvm.sock --rwroot /path/to/rootfs.qcow2 --initrd /path/to/initrd -p "root=/dev/vda1" /path/to/vmlinuz
```
