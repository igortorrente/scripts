#!/bin/sh

export DISPLAY=:0
#export LOCAL_VIRGL='/tmp/local'
export LOCAL_VIRGL='/home/igor/.local'
export PATH="$PATH:${LOCAL_VIRGL}/bin"
export PKG_CONFIG_PATH="${LOCAL_VIRGL}/lib/x86_64-linux-gnu/pkgconfig:${LOCAL_VIRGL}/share/pkgconfig"
export LD_LIBRARY_PATH="${LOCAL_VIRGL}/lib:${LOCAL_VIRGL}/lib/x86_64-linux-gnu"
export VK_ICD_FILENAMES="${LOCAL_VIRGL}/share/vulkan/icd.d/intel_icd.x86_64.json"
#export VK_ICD_FILENAMES="${LOCAL_VIRGL}/share/vulkan/icd.d/lvp_icd.x86_64.json"

PS1="[VIRGL ENV] $PS1"
